package com.example.sortsurname;
import java.util.ArrayList;
import java.util.Comparator;

class SortUsingLastNames {

    static void sortLast(ArrayList<String> al) {
        al.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                String[] split1 = o1.split(" ");
                String[] split2 = o2.split(" ");
                String lastName1 = split1[1];
                String lastName2 = split2[1];
                if (lastName1.compareTo(lastName2) > 0) {
                    return 1;
                } else {
                    return - 1;
                }
            }
        });
        System.out.println(al);
    }

    public static void main(String[] args) {
        ArrayList<String> al = new ArrayList<String>();
        al.add("Tristan Vandevelde");
        al.add("Elke Boonen");
        al.add("Collin Van der vorst");
        al.add("Charlie Beirnaert");
        SortUsingLastNames i = new SortUsingLastNames();
        System.out.println("Sorted using Last Name");
        sortLast(al);
    }

}